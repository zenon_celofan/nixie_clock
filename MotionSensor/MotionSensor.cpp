#include "MotionSensor.h"
#include "bluepill.h"


MotionSensor::MotionSensor(u8 _motion_detection_pin, BitAction _active_state)
: feedback_pin(_motion_detection_pin, GPIO_Mode_IN_FLOATING, DISABLE)
, active_state(_active_state) {
}


bool MotionSensor::is_motion_detected(void) {
	if (feedback_pin.digital_read() == active_state) {
		return true;
	} else {
		 return false;
	}
} //is_motion_detected()
