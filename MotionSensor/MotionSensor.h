#ifndef MOTIONSENSOR_H_
#define MOTIONSENSOR_H_

#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"

class MotionSensor {
public:
	MotionSensor(u8 _motion_detection_pin, BitAction _active_state = Bit_SET);
	bool is_motion_detected(void);

private:
	Pin feedback_pin;
	BitAction active_state;
};

#endif /* MOTIONSENSOR_H_ */
