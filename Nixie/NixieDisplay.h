#ifndef NIXIEDISPLAY_H_
#define NIXIEDISPLAY_H_

#include "bluepill.h"
#include "NixieTube.h"
#include "stm32f10x.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "bluepill.h"


#define NUMBER_OF_NIXIE_TUBES			4


class NixieDisplay {

private:

	NixieTube * nixie_tube[NUMBER_OF_NIXIE_TUBES]; //{{PA4}, {PA5}, {PA6}, {PA7}};
	bool 	digit_active_status[NUMBER_OF_NIXIE_TUBES];

	void init_auto_refresh(void);

public:

    volatile uint8_t digit[NUMBER_OF_NIXIE_TUBES];
	bool 	turned_on;
	bool	balance_mode_active;

	NixieDisplay(uint8_t anode_pins[], uint8_t kathode_pins[]);
	void refresh(void);
	void turn_on(void);
	void turn_off(void);

	void show(uint8_t * array_with_digits_param, uint8_t number_of_digits_param, bool blank_leading_zero = false);
	friend void TIM2_IRQHandler(void);

};

#endif /* NIXIEDISPLAY_H_ */
