#include <stdio.h>
#include <stdlib.h>
#include <NixieDisplay.h>
#include <NixieTube.h>
#include "stm32f10x_conf.h"
#include "Led.h"
#include "Rtc.h"



NixieDisplay::NixieDisplay(uint8_t anode_pins[], uint8_t kathode_pins[4])
	: digit{0, 0, 0, 0}
{
	for (uint8_t i = 0; i < NUMBER_OF_NIXIE_TUBES; ++i) {
		nixie_tube[i] = new NixieTube(anode_pins[i], kathode_pins[0], kathode_pins[1], kathode_pins[2], kathode_pins[3]);
	}
	turned_on = true;
	balance_mode_active = false;

	init_auto_refresh();

    turn_on();

} //NixieDisplay()


void NixieDisplay::turn_on(void) {

	turned_on = true;

} //turn_on()


void NixieDisplay::turn_off(void) {

	turned_on = false;

	for (uint8_t i = 0; i < NUMBER_OF_NIXIE_TUBES; i++) {
		nixie_tube[i]->turn_off();
	}

} //turn_off()


void NixieDisplay::refresh(void) {
	if (turned_on == true) {
		for (uint8_t i = 0; i < NUMBER_OF_NIXIE_TUBES; i++) {
			if (digit_active_status[i] == true) {
				nixie_tube[i]->display_digit(digit[i]);
			} else {
				nixie_tube[i]->turn_off();
			}
		}
	}
} //refresh()


void NixieDisplay::init_auto_refresh(void) {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    // if MCU frequency 72 MHz, prescaler of value 72 will make 1us counter steps
    timerInitStructure.TIM_Prescaler = 72;
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Period = 1000; // will get irq each 1ms on TIMx->CNT overflow
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM2, &timerInitStructure);
    TIM_Cmd(TIM2, ENABLE);

    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
    /*for more accuracy irq priority should be higher, but now its default*/
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x05;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    NVIC_Init(&NVIC_InitStruct);
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
} //init_auto_refresh()


void NixieDisplay::show(uint8_t * _array_with_digits, uint8_t _number_of_digits, bool blank_leading_zero) {
	if ((_array_with_digits[0] == 0) && (blank_leading_zero == true)) {
		digit[0] = 11;  //turn off
	} else {
		digit[0] = _array_with_digits[0];
	}
	for (int i = 1; i < _number_of_digits; i++) {
			digit[i] = _array_with_digits[i];
	}
} //show()
