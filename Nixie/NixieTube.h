#ifndef NIXIETUBE_H_
#define NIXIETUBE_H_

#include "stm32f10x.h"
#include "bluepill.h"
#include "Pin.h"


class NixieTube {

	Pin anode_pin;
	Pin bcd_A_pin;
	Pin bcd_B_pin;
	Pin bcd_C_pin;
	Pin bcd_D_pin;

	//void init_pins(void);


public:

	NixieTube(uint8_t anode_pin_param, uint8_t bdc_A_pin_param = PB12, uint8_t bcd_B_pin_param = PB14, uint8_t bcd_C_pin_param = PB15, uint8_t bcd_D_pin_param = PB13);
	void display_digit(uint8_t digit);
	void turn_on(void);
	void turn_off(void);

};



#endif /* NIXIETUBE_H_ */
