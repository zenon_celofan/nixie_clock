#include <ESP8266WiFi.h>
#include <time.h>

const char* ssid = "time";   
const char* password = "whatisthetime";

#define LED_PIN   2   //D0

int time_zone = 0;
int date_swing_time = 0;



void setup() {
  Serial.begin(9600);
  delay(10);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  WiFi.begin(ssid, password);

  //Serial.print("connecting");

  //while(WiFi.status() != WL_CONNECTED) {
  //  delay(500);
    //Serial.print('.');
  //}

  //digitalWrite(LED_PIN, HIGH);
  //Serial.println();
  //Serial.println(WiFi.localIP());

  configTime(time_zone, date_swing_time, "pool.ntp.org", "time.nist.gov");
  //Serial.print("waiting for internet time");

  //while(!time(nullptr)) {
    //Serial.print(".");
    //delay(1000);
  //}
  //Serial.println();
  digitalWrite(LED_PIN, LOW);

}

void loop()
{
  if (WiFi.status() == WL_CONNECTED) {
    digitalWrite(LED_PIN, LOW);
    time_t now = time(nullptr);
    struct tm* p_tm = localtime(&now);  
  
    Serial.print('!');
    
    if (p_tm->tm_hour < 10) {
      Serial.print('0');
    }
    Serial.print(p_tm->tm_hour);
    
    Serial.print(':');

    if (p_tm->tm_min < 10) {
      Serial.print('0');
    }
    Serial.print(p_tm->tm_min);
    
    Serial.print(':');

    if (p_tm->tm_sec < 10) {
      Serial.print('0');
    }
    Serial.print(p_tm->tm_sec);
    
    Serial.println();
  } else {
    digitalWrite(LED_PIN, HIGH);
  }

  delay(1000);
}

