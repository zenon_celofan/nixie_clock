#include <stdio.h>
#include <stdlib.h>
#include "bluepill.h"
#include "IRQHandler.h"
#include "Led.h"
#include "NixieDisplay.h"
#include "Serial.h"
#include "IRQHandler.h"
#include "Rtc.h"
#include "Micros.h"


extern NixieDisplay nixie_display;


//nixie display refresh
void TIM2_IRQHandler(void) {
	static uint8_t current_digit = 0;

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

	if (nixie_display.balance_mode_active == true) {
		nixie_display.digit[current_digit] += 1;
		if (nixie_display.digit[current_digit] > 9) {
			nixie_display.digit[current_digit] = 0;
		}
	}

	nixie_display.digit_active_status[current_digit] = false;

	current_digit++;
	current_digit = current_digit % NUMBER_OF_NIXIE_TUBES;

	nixie_display.digit_active_status[current_digit] = true;

	nixie_display.refresh();
} //TIM2_IRQHandler


void EXTI0_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line0);
} //EXTI0_IRQHandler()


void EXTI1_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line1);
} //EXTI1_IRQHandler()


void EXTI2_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line2);
} //EXTI2_IRQHandler()


void EXTI3_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line3);
} //EXTI3_IRQHandler()


void EXTI4_IRQHandler(void) {
	EXTI_ClearITPendingBit(EXTI_Line4);
} //EXTI4_IRQHandler()


//touch buttons handler
void EXTI9_5_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line5) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line5);
	} else if (EXTI_GetITStatus(EXTI_Line6) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line6);
	} else if (EXTI_GetITStatus(EXTI_Line7) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line7);
	} else if (EXTI_GetITStatus(EXTI_Line8) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line8);
	} else if (EXTI_GetITStatus(EXTI_Line9) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line9);
	}
} //EXTI9_5_IRQHandler()

void EXTI15_10_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line10) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line10);
	} else if (EXTI_GetITStatus(EXTI_Line11) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line11);
	} else if (EXTI_GetITStatus(EXTI_Line12) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line12);
	} else if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line13);
	} else if (EXTI_GetITStatus(EXTI_Line14) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line14);
	} else if (EXTI_GetITStatus(EXTI_Line15) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line15);
	}
} //EXTI15_10_IRQHandler()


void RTC_IRQHandler(void) {
	RTC_ClearITPendingBit(RTC_IT_SEC);
} //RTC_IRQHandler();


void PVD_IRQHandler(void) {};
void TAMPER_IRQHandler(void) {};
void RTCAlarm_IRQHandler(void) {};
void USBWakeUp_IRQHandler(void) {};
