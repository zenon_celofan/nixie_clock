#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "NixieDisplay.h"
#include "bluepill.h"
#include "millis.h"
#include "micros.h"
#include "Led.h"
#include "stm32f10x_rtc.h"
#include "Pin.h"
#include "stm32f10x_dma.h"
#include "MotionSensor.h"
#include "Serial.h"
#include "DS3231.h"
#include "timers.h"
#include "TouchButton.h"
#include "misc.h"


#define TIME_REFRESH_PERIOD_MS						1000
#define TIME_SYNC_REQUEST_AFTER_MS					10000
#define ESP8266_COMMUNICATION_TIMEOUT_MS			1000
#define DISPLAY_SHUTDOWN_AFTER_NO_MOVEMENT_MS		900000			//15min
#define DISPLAY_BALANCE_MODE_TIME_START_MS			10000 //10800000		//3h
#define DISPLAY_BALANCE_MODE_TIME_STOP_MS			20000 //12600000		//3,5h

void display_manager(void);
void time_manager(void);
void motion_sensor_manager(void);
void board_led_manager(void);
void esp8266_manager(void);
void update_time(void);
void buttons_manager(void);
void debug_serial_manager(void);


uint8_t anodes[] = {PB5, PB4, PB3, PA15};
uint8_t cathodes[] = {PB15, PB13, PB12, PB14};
NixieDisplay nixie_display(anodes, cathodes);

DS3231	dallas_rtc(I2C1);


MotionSensor motion_sensor(PA9, Bit_SET);
Led 	pc13_led(PC13, Bit_RESET);

Serial	debug_serial(USART3);
Serial	esp8266_serial(USART2);
Pin		esp8266_reset_pin(PA7, Bit_RESET);
TouchButton hours_up(PA12, Bit_SET, DISABLE);
TouchButton hours_down(PA11, Bit_SET, DISABLE);
TouchButton minutes_up(PA10, Bit_SET, DISABLE);
TouchButton minutes_down(PB8, Bit_SET, DISABLE);

Timestamp dallas_time;

struct main_time_struct {
	uint8_t time[4] = {8, 8, 8, 8};
	bool is_motion_detected = true;
	uint32_t last_motion_detected_timestamp = 0;
	bool blank_rtc_leading_zero = true;
	uint32_t millis_at_last_sync = 0;
	bool time_sync_request = true;
} main_time_structure;


void lamp_test(void);


void main() {

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	//deinit
	esp8266_reset_pin.digital_write(Bit_SET);

	//init
	millis_init();

	debug_serial.send_it("\n\n\n*** nixie_clock - RESET ***\n\n");
//	esp8266_serial.send("ready\n");

    pc13_led.turn_off();

	lamp_test();

	Timers<7> timers;

	timers.attach(0, 1000, time_manager);
	timers.attach(1, 1000, display_manager);
	timers.attach(2, 500, motion_sensor_manager);
	timers.attach(3, 1000, board_led_manager);
	timers.attach(4, 1000, esp8266_manager);
	timers.attach(5, 50, buttons_manager);
	timers.attach(6, 1000, debug_serial_manager);

	dallas_rtc.get_time();

	while (1) {
		timers.process();
	}
} //main()



void buttons_manager(void) {
	static BitAction minutes_up_button_last_state = Bit_RESET;
	static BitAction minutes_down_button_last_state = Bit_RESET;
	static BitAction hours_up_button_last_state = Bit_RESET;
	static BitAction hours_down_button_last_state = Bit_RESET;

	if (hours_up.is_button_pressed() == true) {
		if (hours_up_button_last_state == Bit_RESET) {
			debug_serial.send("\nhours up\n");
			hours_up_button_last_state = Bit_SET;
			dallas_rtc.hours_increment();
			if (main_time_structure.time[1] == 9) {
				main_time_structure.time[1] = 0;
				main_time_structure.time[0]++;
			} else if ((main_time_structure.time[0] == 2) && (main_time_structure.time[1] == 3)) {
				main_time_structure.time[0] = 0;
				main_time_structure.time[1] = 0;
			} else {
				main_time_structure.time[1]++;
			}
			display_manager();
			nixie_display.refresh();
		}
	} else {
		hours_up_button_last_state = Bit_RESET;
	}

	if (hours_down.is_button_pressed() == true) {
		if (hours_down_button_last_state == Bit_RESET) {
			debug_serial.send("\nhours down\n");
			hours_down_button_last_state = Bit_SET;
			dallas_rtc.hours_decrement();
			if (main_time_structure.time[1] == 0) {
				if (main_time_structure.time[0] == 0) {
					main_time_structure.time[0] = 2;
					main_time_structure.time[1] = 3;
				} else {
					main_time_structure.time[0]--;
					main_time_structure.time[1] = 9;
				}
			} else {
				main_time_structure.time[1]--;
			}
			display_manager();
			nixie_display.refresh();
		}
	} else {
		hours_down_button_last_state = Bit_RESET;
	}

	if (minutes_up.is_button_pressed() == true) {
		if (minutes_up_button_last_state == Bit_RESET) {
			debug_serial.send("\nminutes up\n");
			minutes_up_button_last_state = Bit_SET;
			dallas_rtc.minutes_increment();
			if (main_time_structure.time[3] == 9) {
				main_time_structure.time[3] = 0;
				if (main_time_structure.time[2] == 5) {
					main_time_structure.time[2] = 0;
				} else {
					main_time_structure.time[2]++;
				}
			} else {
				main_time_structure.time[3]++;
			}
			display_manager();
			nixie_display.refresh();
		}
	} else {
		minutes_up_button_last_state = Bit_RESET;
	}

	if (minutes_down.is_button_pressed() == true) {
		if (minutes_down_button_last_state == Bit_RESET) {
			debug_serial.send("\nminutes down\n");
			minutes_down_button_last_state = Bit_SET;
			dallas_rtc.minutes_decrement();
			if (main_time_structure.time[3] == 0) {
				main_time_structure.time[3] = 9;
				if (main_time_structure.time[2] == 0) {
					main_time_structure.time[2] = 5;
				} else {
					main_time_structure.time[2]--;
				}
			} else {
				main_time_structure.time[3]--;
			}
			display_manager();
			nixie_display.refresh();
		}
	} else {
		minutes_down_button_last_state = Bit_RESET;
	}
} //buttons_manager()


void display_manager(void) {
	if (main_time_structure.is_motion_detected == true) {
		nixie_display.turn_on();
		nixie_display.show(main_time_structure.time, 4, main_time_structure.blank_rtc_leading_zero);
	} else {
		nixie_display.turn_off();
	}
} //time_manager()


void time_manager(void) {
	uint32_t time_since_last_update;

	dallas_time = dallas_rtc.get_time();

	if (main_time_structure.time_sync_request == false) {
		time_since_last_update = millis() - main_time_structure.millis_at_last_sync;
		if (time_since_last_update >= TIME_SYNC_REQUEST_AFTER_MS) {
			main_time_structure.time_sync_request = true;
		}
	}

	//update display digits
	main_time_structure.time[0] = (uint8_t)  dallas_time.hours / 10;
	main_time_structure.time[1] = (uint8_t)  dallas_time.hours % 10;
	main_time_structure.time[2] = (uint8_t)  dallas_time.minutes / 10;
	main_time_structure.time[3] = (uint8_t)  dallas_time.minutes % 10;
} //time_manager()


void motion_sensor_manager(void) {
	if (motion_sensor.is_motion_detected() == true) {
		if (main_time_structure.is_motion_detected == false) {
			main_time_structure.is_motion_detected = true;
			main_time_structure.last_motion_detected_timestamp = millis();
		}
	} else if (main_time_structure.is_motion_detected == true) {
		if (millis() - main_time_structure.last_motion_detected_timestamp >= DISPLAY_SHUTDOWN_AFTER_NO_MOVEMENT_MS) {
			main_time_structure.is_motion_detected = false;
		}
	}
} //motion_sensor_manager()


void board_led_manager(void) {
	pc13_led.toggle();
} //board_led_manager()


void esp8266_manager(void) {
	if (main_time_structure.time_sync_request == true) {
//		if (esp8266_serial.is_rx_busy == false) {
//			esp8266_serial.send_it("time_request\n");
//			esp8266_serial.receive_it_with_callback(update_time);
//		}
	}
} //esp8266_manager()


void update_time(void) {
//	Timestamp time;
//
//	if (esp8266_serial.rx_buf[0] == '!') {
//		time.hours = (esp8266_serial.rx_buf[2]-0x30)*10 + (esp8266_serial.rx_buf[3]-0x30);
//		time.minutes = (esp8266_serial.rx_buf[5]-0x30)*10 + (esp8266_serial.rx_buf[6]-0x30);
//		time.seconds = (esp8266_serial.rx_buf[8]-0x30)*10 + (esp8266_serial.rx_buf[9]-0x30);
//		time.time_format = TIME_FORMAT_24H;
//		debug_serial.send(esp8266_serial.rx_buf);
//		dallas_rtc.set_time(time);
//	}
//
//	main_time_structure.millis_at_last_sync = millis();
//	main_time_structure.time_sync_request = false;
} //update_time()


void debug_serial_manager(void) {
	char message[100];

	sprintf(message, "dallas time: %u %u %u:%u:%u\n\n", dallas_time.time_format, dallas_time.am_pm, dallas_time.hours, dallas_time.minutes, dallas_time.seconds);
	debug_serial.send(message);
} //debug_serial_manager()


void lamp_test(void) {
	uint8_t t[4] = {1, 2, 3, 4};

	nixie_display.show(t, 4, false);
	delay(1000);

	for (uint8_t i = 0; i < 10; ++i) {
		t[0] = i;
		t[1] = i;
		t[2] = i;
		t[3] = i;
		nixie_display.show(t, 4, false);
		delay(100);
	}
} //lamp_test()
