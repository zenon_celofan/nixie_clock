#nixie_clock

GitLab repository:
https://gitlab.com/zenon_celofan/nixie_clock.git

This repository contains sub-module (bluepill).
To clone nixie_clock with all submodules, use:

`Git clone --recursive https://gitlab.com/zenon_celofan/nixie_clock.git`